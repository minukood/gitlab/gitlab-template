# gitlab-template

Templates for GitLab pipelines.  

Use as inspiration and help keep pipeline configurations clean, lean and mean.  
i.e. Your ultimate code producing machines.

## [Job templates](./templates.md)

A collection of job templates that can be included and used in pipelines.

Please read [templates.md](./templates.md) for complete documentation.

### Usage

```yaml
include:
  - project: 'minukood/gitlab/gitlab-template'
    file: '/templates.yml'
```