# docker-app

Pipeline for docker application.

### Usage

```yaml
include:
  - project: 'minukood/gitlab/gitlab-template'
    file: '/pipelines/docker-app.yml'
```