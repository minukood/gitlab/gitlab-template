#!/usr/bin/env bash

set -Eeuo pipefail

readonly BASE_DIR=$( cd "$(dirname "${0}")" ; pwd -P )

verify() {
  local FILE=""${1}

  # Get hash before build
  local BEFORE_HASH
  BEFORE_HASH=$( sha256sum "${FILE}" | cut -d" " -f1)

  # build
  "${BASE_DIR}/build.sh"

  # Get hash after build
  local AFTER_HASH
  AFTER_HASH=$( sha256sum "${FILE}" | cut -d" " -f1)

  # Compare hashes
  if [ ! "${BEFORE_HASH}" == "${AFTER_HASH}" ]; then
    echo "${FILE} does not appear to have been built correctly."
    echo "Please fix this immediately."
    exit 1
  fi
}

verify "${BASE_DIR}/../templates.yml"
verify templates/test.yml "${BASE_DIR}/test.yml"