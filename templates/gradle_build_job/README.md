Runs `./gradlew --build-cache $GRADLE_BUILD_CMD`.  
By default `$GRADLE_BUILD_CMD` is `assemble` and `./gradlew --build-cache assemble` will be run.

Artifacts:
- build/libs/
- docker/

| variable         | description
|---               | ---
| GRADLE_BUILD_CMD | Gradle build command, can be overridden.