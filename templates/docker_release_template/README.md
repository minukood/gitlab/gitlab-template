Template for tagging docker images.
Tags `$DOCKER_IMAGE:$CI_COMMIT_SHA` as `$DOCKER_IMAGE:$TAG` and pushes to docker registry.

| variable | description
|---       | ---
| TAG      | Tag added to image