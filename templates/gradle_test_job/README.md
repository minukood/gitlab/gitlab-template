Runs `./gradlew $GRADLE_TEST_CMD`.  
By default `GRADLE_TEST_CMD` is `check` and `./gradlew check` will be run.

| variable        | description
|---              | ---
| GRADLE_TEST_CMD | Gradle test command, can be overridden.