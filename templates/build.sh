#!/usr/bin/env bash

set -Eeuo pipefail

readonly BASE_DIR=$( cd "$(dirname "${0}")" ; pwd -P )

#

ALL_VARS=""
ALL_JOBS=""

_add_global_variables() {
    local TEMPLATE_NAME="${1}"
    local VARIABLES="${2}"

    ALL_VARS="${ALL_VARS}  # ${TEMPLATE_NAME}\n${VARIABLES}\n"
}

_add_job() {
    local TEMPLATE_NAME="${1}"
    local JOB="${2}"

    ALL_JOBS="${ALL_JOBS}\n${TEMPLATE_NAME}:\n${JOB}\n"
}

#

ALL_TEST_VARS=""
ALL_TEST_JOBS=""

_add_global_test_variables() {
    local TEMPLATE_NAME="${1}"
    local TEST_VARIABLES="${2}"

    ALL_TEST_VARS="${ALL_TEST_VARS}  # ${TEMPLATE_NAME}\n${TEST_VARIABLES}\n"
}

_add_test_job() {
    local NAME="${1}"
    local TEST_JOB_USAGE="${2}"

    ALL_TEST_JOBS="${ALL_TEST_JOBS}\n${NAME}_test:\n${TEST_JOB_USAGE}\n  stage: template test\n"
}

#

ALL_README=""

_add_readme() {
    local README="${1}"

    ALL_README="${ALL_README}${README}"
}

#

_log_error() {
    local MSG="${1}"
    (>&2 echo "Error: ${MSG}")
}

#

markdown_yaml_code() {
    local CODE="${1}"
    echo -en "\n\`\`\`yaml\n${CODE}\n\`\`\`\n"
}

#
# Create README text for template
#
template_readme() {
    local TEMPLATE_NAME="${1}"
    local EXTENDS="${2}"
    local TEMPLATE_TYPE="${3}"
    local README_FILE="${4}"
    local VARIABLES="${5}"
    local TEST_JOB_USAGE="${6}"

    local README=""

    # Heading
    README="${README}\n## ${TEMPLATE_NAME}\n"

    # Extends
    if [ -n "${EXTENDS}" ]; then
        README="${README}\nExtends: [\`${EXTENDS}\`](#$(echo ${2} | tr -d '.'))\n"
    fi

    # Type
    README="${README}\nType: \`${TEMPLATE_TYPE}\`\n"

    # README.md contents
    if [ -f "${README_FILE}" ]; then
        README="${README}\n$(cat "${README_FILE}")\n"
    fi

    # Global variables
    if [ -n "${VARIABLES}" ]; then
        README="${README}$(markdown_yaml_code "variables:\n${VARIABLES}")\n"
    fi

    # Example usage
    if [ -n "${TEST_JOB_USAGE}" ]; then
        README="${README}$(markdown_yaml_code "example:\n${TEST_JOB_USAGE}")\n"
    fi

    _add_readme "${README}"
}

#
# Read variables: part of yaml file
#
_template_read_variables() {
    local FILE="${1}"

    if [ ! -f "${FILE}" ]; then
        (>&2 echo "Error: "${FILE}" does not exist")
        return 1
    fi

    cat "${FILE}" | sed -n '/^variables:$/,/^[^ ].*$/{//!p;}'
}

#
# Read job: part of yaml file
#
_template_read_job() {
    local FILE="${1}"

    if [ ! -f "${FILE}" ]; then
        echo "Error: "${FILE}" does not exist"
        exit 1
    fi

    cat "${FILE}" | sed -n '/^job:$/,/^[^ ].*$/{//!p;}'
}

_job_extends() {
    local JOB="${1}"

    echo "${JOB}" | sed -n 's/^  extends: \(.*\)/\1/p'
}

#

_template() {
    local TEMPLATE_DIR="${1}"
    local NAME="${2}"

    local JOB_FILE="${TEMPLATE_DIR}/job.yml"
    local README_FILE="${TEMPLATE_DIR}/README.md"
    local TEST_FILE="${TEMPLATE_DIR}/test.yml"

    local TEMPLATE_NAME=".${NAME}"
    local TEMPLATE_TYPE="Unknown"

    case "${NAME}" in
        *_template)
          TEMPLATE_TYPE="Template"
          ;;
        *_job)
          TEMPLATE_TYPE="Job"
          ;;
        *)
          TEMPLATE_TYPE="Template (default)"
          TEMPLATE_NAME="${TEMPLATE_NAME}_template"
          ;;
    esac

    echo "${NAME}"

    local JOB=$(_template_read_job "${JOB_FILE}")

    if [ -z "${JOB}" ]; then
        _log_error "No job contents defined."
        exit 1
    fi

    _add_job "${TEMPLATE_NAME}" "${JOB}"

    # Global variables defined in job
    local VARIABLES=$(_template_read_variables "${JOB_FILE}")
    if [ -n "${VARIABLES}" ]; then
        _add_global_variables "${TEMPLATE_NAME}" "${VARIABLES}"
    fi

    local TEST_JOB_USAGE=""
    if [ -f "${TEST_FILE}" ]; then
        echo "  - Test"

        local TEST_VARIABLES=$(_template_read_variables "${TEST_FILE}")
        if [ -n "${TEST_VARIABLES}" ]; then
            _add_global_test_variables "${TEMPLATE_NAME}" "${TEST_VARIABLES}"
        fi

        local TEST_JOB=$(_template_read_job "${TEST_FILE}")
        TEST_JOB_USAGE="  extends: ${TEMPLATE_NAME}"
        if [ -n "${TEST_JOB}" ]; then
            TEST_JOB_USAGE="${TEST_JOB_USAGE}\n${TEST_JOB}"
        fi
        _add_test_job "${NAME}" "${TEST_JOB_USAGE}"
    fi

    local JOB_EXTENDS=$(_job_extends "${JOB}")

    template_readme "${TEMPLATE_NAME}" "${JOB_EXTENDS}" "${TEMPLATE_TYPE}" "${README_FILE}" "${VARIABLES}" "${TEST_JOB_USAGE}"
}

#

build() {
    echo "Building in: ${BASE_DIR}"
    cd "${BASE_DIR}"

    local GENERATED_WARN="This file is generated using /templates/build.sh"

    _add_readme "\n[comment]: # (${GENERATED_WARN})\n"

    # Loop over all templates
    for TEMPLATE_DIR in *; do
        if [ -d "${TEMPLATE_DIR}" ]; then # Is directory
            _template "${BASE_DIR}/${TEMPLATE_DIR}" "${TEMPLATE_DIR}"
        fi
    done

    # Build templates.yml
    echo -en "#\n# ${GENERATED_WARN}\n#\n\nvariables:\n${ALL_VARS}${ALL_JOBS}" > "${BASE_DIR}/../templates.yml"
    # Build test.yml
    echo -en "#\n# ${GENERATED_WARN}\n#\n\nvariables:\n${ALL_TEST_VARS}${ALL_TEST_JOBS}" > "${BASE_DIR}/test.yml"
    # Build templates.md
    echo -en "${ALL_README}" > "${BASE_DIR}/../templates.md"
}

build