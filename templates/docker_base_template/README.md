Base template for docker jobs.
Empties `dependencies` to override default.

> https://docs.gitlab.com/ee/ci/yaml/#dependencies
> By default, all artifacts from all previous stages are passed, but you can use the dependencies parameter to define a limited list of jobs (or no jobs) to fetch artifacts from.