
[comment]: # (This file is generated using /templates/build.sh)

## .docker_base_template

Type: `Template`

Base template for docker jobs.
Empties `dependencies` to override default.

> https://docs.gitlab.com/ee/ci/yaml/#dependencies
> By default, all artifacts from all previous stages are passed, but you can use the dependencies parameter to define a limited list of jobs (or no jobs) to fetch artifacts from.

```yaml
example:
  extends: .docker_base_template
  image: alpine
  script: 'true'
```

## .docker_build_job

Extends: [`.docker_template`](#docker_template)

Type: `Job`

Build and push docker image in `$DOCKER_DIR` to `$DOCKER_IMAGE:$CI_COMMIT_SHA`.

```yaml
variables:
  DOCKER_DIR: '.' # Override if Dockerfile is not in base directory
```

## .docker_release_latest_job

Extends: [`.docker_release_template`](#docker_release_template)

Type: `Job`

Tag and push `latest` tag on main or master branch.

## .docker_release_tag_job

Extends: [`.docker_release_template`](#docker_release_template)

Type: `Job`

Tag and push `$CI_COMMIT_TAG` tag on git tags.

## .docker_release_template

Extends: [`.docker_template`](#docker_template)

Type: `Template`

Template for tagging docker images.
Tags `$DOCKER_IMAGE:$CI_COMMIT_SHA` as `$DOCKER_IMAGE:$TAG` and pushes to docker registry.

| variable | description
|---       | ---
| TAG      | Tag added to image

## .docker_template

Extends: [`.docker_base_template`](#docker_base_template)

Type: `Template`

Base template for docker jobs.

```yaml
variables:
  # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_IMAGE: $CI_REGISTRY_IMAGE
  # Example when using external docker registry
  #DOCKER_IMAGE: 'my.docker.registry/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME'
```

## .gradle_build_job

Extends: [`.gradle_template`](#gradle_template)

Type: `Job`

Runs `./gradlew --build-cache $GRADLE_BUILD_CMD`.  
By default `$GRADLE_BUILD_CMD` is `assemble` and `./gradlew --build-cache assemble` will be run.

Artifacts:
- build/libs/
- docker/

| variable         | description
|---               | ---
| GRADLE_BUILD_CMD | Gradle build command, can be overridden.

## .gradle_template

Extends: [`.docker_base_template`](#docker_base_template)

Type: `Template`

Base template for Java and Gradle jobs.

```yaml
variables:
  # Use the latest openjdk image by default
  JAVA_IMAGE: openjdk:latest
  # Disable the Gradle daemon for Continuous Integration servers as correctness
  # is usually a priority over speed in CI environments. Using a fresh
  # runtime for each build is more reliable since the runtime is completely
  # isolated from any previous builds.
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"
```

```yaml
example:
  extends: .gradle_template
  script: java -version
```

## .gradle_test_job

Extends: [`.gradle_template`](#gradle_template)

Type: `Job`

Runs `./gradlew $GRADLE_TEST_CMD`.  
By default `GRADLE_TEST_CMD` is `check` and `./gradlew check` will be run.

| variable        | description
|---              | ---
| GRADLE_TEST_CMD | Gradle test command, can be overridden.

## .npm_template

Extends: [`.docker_base_template`](#docker_base_template)

Type: `Template`

Base template for Node and npm jobs.

```yaml
variables:
  # Use the latest node image by default
  NODE_IMAGE: node:latest
```

```yaml
example:
  extends: .npm_template
  script: node --version
```

## .npm_test_job

Extends: [`.npm_template`](#npm_template)

Type: `Job`

Runs `npm test`.  

## .trigger_template

Extends: [`.docker_base_template`](#docker_base_template)

Type: `Template`

Template for triggering pipelines in other projects.

Uses [pipeline-trigger](https://gitlab.com/finestructure/pipeline-trigger).

```yaml
example:
  extends: .trigger_template
  script: command -v trigger
```
